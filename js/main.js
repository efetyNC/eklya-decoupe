$(document).ready(function(){

    /* animation sticky header */

    $header = $('.site-header').height();
    $('main').css('padding-top',$header);

    $surheaderheight = $('.site-header .surheader').height();
    $('.site-header .surheader').css('height',$surheaderheight);

    $(window).scroll(function(){
        var winTop = $(window).scrollTop();
        if(winTop >= $header){
            $(".site-header").addClass("sticky-header");
            $header = $('.site-header .menu').height();
            $('main').css('padding-top',$header);
        }else{
            $(".site-header").removeClass("sticky-header");
            $header = $('.site-header').height();
            $('main').css('padding-top',$header);
        }//if-else
    });//win func.

    /* fin animation sticky header */


    $('.slick-liste').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /* Megamenu on hover */

    $('.nv1>span').on('mouseover', function () {
        if ($(window).width() > 991) {
            if ($(this).parent().siblings('.dropdown_main').length !== 0) {
                if ($(this).parent().siblings('.dropdown_main').hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).parent().siblings('.dropdown_main').slideUp(300).removeClass('active');
                } else {
                    $('.navbar-nav .nv1>span.active').removeClass('active');
                    $('.dropdown_main').hide().removeClass('active');
                    $(this).addClass('active');
                    $(this).parent().siblings('.dropdown_main').slideDown(300).addClass('active');
                };
            } else {
                $('.navbar-nav .nv1>span.active').removeClass('active');
                $('.dropdown_main').slideUp(300).removeClass('active');
            };
        };
    });

    /* version mobile */

    var $hamburger = $(".hamburger");
    $hamburger.on("click", function(e) {
        $hamburger.toggleClass("is-active");
        // Do something else, like open/close menu
    });

    $('.nav-item>i').on('click', function () {
        if ($(window).width() < 992) {
            if ($(this).siblings('.dropdown_main').length !== 0) {
                if ($(this).hasClass('fa-angle-down')) {
                    $(this).siblings('.dropdown_main').slideUp(300).removeClass('active');
                    $(this).removeClass('fa-angle-down').addClass('fa-angle-right');
                } else {
                    $('.dropdown_main').slideUp(300).removeClass('active');
                    $(this).siblings('.dropdown_main').slideDown(300).addClass('active');
                    $(this).removeClass('fa-angle-right').addClass('fa-angle-down');
                }
            } else {
                $('.dropdown_main').slideUp(300).removeClass('active');
            };
        };

    });

    $('.navbar-collapse .dropdown_main .second-level i').on('click', function () {
        if ($(this).hasClass('fa-plus')) {
            $('.navbar-collapse .dropdown_main .second-level .fa-minus').removeClass('fa-minus').addClass('fa-plus');
            $('.navbar-collapse .dropdown_main .second-level .third-level').slideUp(300);
            $(this).removeClass('fa-plus').addClass('fa-minus');
            $(this).parents('.second-level').find('.third-level').slideDown(300);
        } else {
            $(this).removeClass('fa-minus').addClass('fa-plus');
            $(this).parents('.second-level').find('.third-level').slideUp(300);
        };
    });

    /* fin version mobile */

    $('.navbar-nav .retour-mobile').on('click', function () {
        $('.dropdown_main').removeClass('active');
    });


    $('.menu .navbar').on('mouseleave', function () {
        $('.dropdown_main').slideUp(300).removeClass('active');
        $('.nv1>span.active').removeClass('active');
    });

    /* fin megamenu on hover */


    /* responsive - from static to slider */

    $slick_slider = $('.programmes-slider');
    settings = {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }
    $slick_slider.slick(settings);

    // reslick only if it's not slick()
    $(window).on('resize', function() {
        if ($(window).width() > 1199) {
            if ($slick_slider.hasClass('responsive-slider')) {
                $slick_slider.slick('unslick');
            }
        } else {
            if ($slick_slider.hasClass('responsive-slider')) {
                return $slick_slider.slick(settings);
            }
        }
    });
    
    
});

